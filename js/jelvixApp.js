'use strict';

angular.module('jelvixApp',[])
.controller('dataCtrl', ['$scope','$http', function($scope, $http) {
    $http.get('http://jsonplaceholder.typicode.com/users').then(successUser, error);
    $http.get('http://jsonplaceholder.typicode.com/posts').then(successPosts, error);
    $http.get('http://jsonplaceholder.typicode.com/comments').then(successComment, error);
    function successUser (data) {
        $scope.userList = data.data;
    }
    function successPosts(data) {
        $scope.postList = data.data;
    }
    function successComment(data) {
        $scope.commentList = data.data;
    }
    function error (data, status) {
        console.log(data);
        console.log(status);
    }

    $scope.selectUser = function (id) {
        $scope.currentUser = id;
    }

    $scope.selectPost = function (id) {
        $scope.currentPost = id;
    }

}]);

